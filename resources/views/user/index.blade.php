@extends('layout')

@section('content')
<h1>{{ $title }}</h1>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{ route("user.edit",["user"=>$user->id]) }}">
                        Edit
                    </a>
                <form method="POST"
                action="{{ route("user.destroy",["user"=>$user->id]) }}">
                @csrf
                @method('DELETE')
                    <button type="submit">Delete</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<a href="{{route('user.create')}}">Create New User</a>




@endsection
