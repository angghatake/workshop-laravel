@extends('layout')

@section('content')

    @if(! @$user) {{--lagi create --}}
    <form method="POST" action="{{ route("user.store") }}">
        @method('POST')
    @else
    <form method="POST"
    action="{{ route("user.update",["user"=>$user->id]) }}">
        @method('PUT')
    @endif

    @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{ @$user->name }}">
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{ @$user->email }}">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="password" value="{{ @$user->password }}">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection